An Improbable CRDT
==================

Improbable is a collection of semantics and algorithms for an efficient operation-based CRDT suitable for peer to peer applications including gossip networks

___This README describes a WIP. All features are TODO___

___Improbable CRDT describes CRDT semantics and algorithms___

___When available, implementations will be listed below___

Outline:
 - Guiding principles
 - Basic CRUD
 - Definitions
 - Serialization syntaxes
 - Security concerns


Guiding principles
------------------

The purpose of Improbable is to describe the semantic primitives and sufficient example algorithms to build a documentcentric CDRT implementation. The CRDT described is an operation-based CRDT that leverages peer connections to retrieve any mappings that aren't trivially computable. An initial schema is being prepared in KDL because the opinions embedded in the syntax pose a low barrier to understanding; schemas in other syntaxes would also be on scope

Basic CRUD
----------

The following operations will be defined for individual documents. The Activity Streams 2.0 Vocabulary is recommended for collection management. Additionally, Activity Pub is the recommended base vocabulary for managing agents via the actor pattern, even when the application isn't "social media"

Create - The create syntax needs to describe various metadata about the author, media type and other aspects of the document, including the hash algorythm for the CRDT and the public key of the original creator. It will also include the initial content of the document and the final hash. Any empty document may be created to poll potential collaborators about their capabilities

Read - The read syntax contains metadata about the requestor, the hash of a state being requested, and a list of the requestor's known hashes of the document in reverse chronological order. A well-formed affirmative response will consist of a map from the first mutually recognizable hash on the list to the requested hash. Note that this may be the create operation. A negative response may be subject to security considerations

Update - An update operation will include metadata about the author and the contribution circumstances, the intial hash, the starting position of the operation in bytes, the contents being deleted if any, the contents being inserted if any, and the final hash if different. An empty update may be sent to initialize a change in hash algorithms or to acknowledge receipt of an update from a peer

Delete - A delete operation signals the end of the issuing agent's involvement with the document and requests that other agents involved with the document collaborate to remove copies of the document from the network

Definitions
-----------

Agent - An agent is software operated by or on behalf of a person in any role

Author - An author is any entity, usually a person, who has contributed to a document

Bytestream - The document as encoded in the agreed upon character representation as stated in the metadata of the create event or the first update of nonzero length by an author listed in the metadata of the create event

Character - Only used in bytestreams, e.g. UTF-8 text, a character is the location of the first character of a substring in a zero-indexed stream. Note that this definition imposes no opinions on text parsing issues such as ligatures and multibyte characters

Create - An event that starts a document of any length. If the length of the document upon creation is 0, its hash is zero and document metadata other than the author information may be undefined or an array of options

Delete - An event that begins a progressive delete, properly read as a deletion request. The document is marked as deleted, locally, and the delete is sent to peers. Peers receiving the deletion request should only notify the agent requesting the deletion of events concerning the deletion (security policy permitting), not events concerning the document, itself 

Event - An event is one of Create, Read, Update, or Delete. Except for the create event, an event that changes the document must not alter metadata and an event that alters metadata must not change the document

Hash - The output of the negotiated collision-resistant hash algorithm when applied to the bytestream of the document. A 64 bit FNV hash is used in testing

Read - An event to syncronize state with a peer

Rope - A binary tree that stores substring information. See (Rope on Wikipedia)[https://wikipedia.org/wiki/Rope_(data_structure)] for general information until specific information relevant to Improbable is available (ROPE/md TODO)

Selector - The property of an update event used to identify the node(s) being altered. When a document format naturally has a tree structure or whitespace is not significant, e.g. KDL- or XML-based formats, selector syntax is preferred over character (location) for indicating the place where the specified update applies

Update - An insertion, deletion, or replacement. Algorithms described in connection to the rope data structure can assist implementations in applying updates to the bytestream efficiently

Serializations syntaxes
-----------------------


Security concerns
-----------------
